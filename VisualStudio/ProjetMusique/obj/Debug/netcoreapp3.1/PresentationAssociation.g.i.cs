﻿#pragma checksum "..\..\..\PresentationAssociation.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8DC50F8BD1731FF6C29FC62E89CA14FF1E586B9D"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using ProjetMusique;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ProjetMusique {
    
    
    /// <summary>
    /// PresentationAssociation
    /// </summary>
    public partial class PresentationAssociation : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\..\PresentationAssociation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Presentation;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\PresentationAssociation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrilleCouleur;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\PresentationAssociation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle ImageArtiste;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\PresentationAssociation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AjoutInfo;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\PresentationAssociation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Grille_Presentation;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\PresentationAssociation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox Grille_Presentation_artiste;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\..\PresentationAssociation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox ListGroupe;
        
        #line default
        #line hidden
        
        
        #line 131 "..\..\..\PresentationAssociation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox ListEvenement;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\..\PresentationAssociation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AjouterInfo;
        
        #line default
        #line hidden
        
        
        #line 150 "..\..\..\PresentationAssociation.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GrilleFond;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ProjetMusique;component/presentationassociation.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\PresentationAssociation.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Presentation = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            
            #line 38 "..\..\..\PresentationAssociation.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_3);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 40 "..\..\..\PresentationAssociation.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_1);
            
            #line default
            #line hidden
            return;
            case 4:
            
            #line 42 "..\..\..\PresentationAssociation.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_2);
            
            #line default
            #line hidden
            return;
            case 5:
            
            #line 44 "..\..\..\PresentationAssociation.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Retour_Menu);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 49 "..\..\..\PresentationAssociation.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.RetourRepertoire);
            
            #line default
            #line hidden
            return;
            case 7:
            this.GrilleCouleur = ((System.Windows.Controls.Grid)(target));
            return;
            case 8:
            this.ImageArtiste = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 9:
            
            #line 84 "..\..\..\PresentationAssociation.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            
            #line 87 "..\..\..\PresentationAssociation.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Modificiation);
            
            #line default
            #line hidden
            return;
            case 11:
            this.AjoutInfo = ((System.Windows.Controls.Button)(target));
            
            #line 91 "..\..\..\PresentationAssociation.xaml"
            this.AjoutInfo.Click += new System.Windows.RoutedEventHandler(this.RenseignerInformation);
            
            #line default
            #line hidden
            return;
            case 12:
            this.Grille_Presentation = ((System.Windows.Controls.Grid)(target));
            return;
            case 13:
            this.Grille_Presentation_artiste = ((System.Windows.Controls.ListBox)(target));
            return;
            case 14:
            this.ListGroupe = ((System.Windows.Controls.ListBox)(target));
            return;
            case 15:
            this.ListEvenement = ((System.Windows.Controls.ListBox)(target));
            return;
            case 16:
            this.AjouterInfo = ((System.Windows.Controls.Button)(target));
            return;
            case 17:
            this.GrilleFond = ((System.Windows.Controls.Grid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

