﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour PresentationGroupe.xaml
    /// </summary>
    public partial class PresentationGroupe : Window
    {
        public Groupe groupe { get; private set; }

        public RepertoireGroupe RepertoireGroupe { get; private set; }
        
        public PresentationGroupe(Groupe groupe, RepertoireGroupe repertoireGroupe)
        {
            InitializeComponent();
            RepertoireGroupe = repertoireGroupe;
            this.groupe = groupe;
            this.Presentation.DataContext = groupe;
            ChangementDeMode();

        }

        private void Retour_Menu(object sender, RoutedEventArgs e)
        {
            RepertoireGroupe.fenetre.Show();
            RepertoireGroupe.Close();
            this.Close();
        }

        private void RetourRepertoire(object sender, RoutedEventArgs e)
        {
            RepertoireGroupe.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ListAssociation.Visibility = Visibility.Hidden;
            ListEvenement.Visibility = Visibility.Hidden;
            Grille_Presentation.Visibility = Visibility.Visible;
            Grille_Presentation_artiste.Visibility = Visibility.Hidden;
            AjoutInfo.Visibility = Visibility.Hidden;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ListAssociation.Visibility = Visibility.Hidden;
            ListEvenement.Visibility = Visibility.Hidden;
            Grille_Presentation.Visibility = Visibility.Hidden;
            Grille_Presentation_artiste.Visibility = Visibility.Visible;          
            AjoutInfo.Visibility = Visibility.Visible;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ListAssociation.Visibility = Visibility.Visible;
            Grille_Presentation.Visibility = Visibility.Hidden;
            ListEvenement.Visibility = Visibility.Hidden;
            Grille_Presentation_artiste.Visibility = Visibility.Hidden;
            AjoutInfo.Visibility = Visibility.Visible;

        }
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ListAssociation.Visibility = Visibility.Hidden;
            Grille_Presentation.Visibility = Visibility.Hidden;
            ListEvenement.Visibility = Visibility.Visible;
            Grille_Presentation_artiste.Visibility = Visibility.Hidden;
            AjoutInfo.Visibility = Visibility.Visible;
        }
        private void RenseignerInformation(object sender, RoutedEventArgs e)
        {
            if (Grille_Presentation_artiste.Visibility == Visibility.Visible)
            {
                AjoutInformation ajoutInformation = new AjoutInformation(groupe, 1,RepertoireGroupe.fenetre);
                ajoutInformation.Show();
            }
            if (ListAssociation.Visibility == Visibility.Visible)
            {
                AjoutInformation ajoutInformation = new AjoutInformation(groupe, 3, RepertoireGroupe.fenetre);
                ajoutInformation.Show();
            }
            if (ListEvenement.Visibility == Visibility.Visible)
            {
                AjoutInformation ajoutInformation = new AjoutInformation(groupe, 4, RepertoireGroupe.fenetre);
                ajoutInformation.Show();
            }
            RepertoireGroupe.Show();
            this.Close();
        }

        private void Modificiation(object sender, RoutedEventArgs e)
        {
            ModificationElement modification = new ModificationElement(groupe);
            modification.Show();
        }
        private void ChangementDeMode()
        {
            if (RepertoireGroupe.fenetre.Mode)
            {
                GrilleFond.Background = Brushes.White;
            }
            else
            {
                GrilleFond.Background = Brushes.Black;
                GrilleCouleur.Background = Brushes.DarkGreen;
            }

            if (RepertoireGroupe.fenetre.Daltonisme != 0)
            {
                var converter = new BrushConverter();
                if (RepertoireGroupe.fenetre.Daltonisme == 1)
                {
                    var brush = (Brush)converter.ConvertFromString("#FF9200A7");
                    GrilleCouleur.Background = brush;
                }
                else
                {
                    var brush = (Brush)converter.ConvertFromString("#FF00AD67");
                    GrilleCouleur.Background = brush;
                }
            }
        }

    }
}
