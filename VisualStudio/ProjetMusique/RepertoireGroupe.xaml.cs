﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour RepertoireGroupe.xaml
    /// </summary>
    public partial class RepertoireGroupe : Window
    {
        

        public Bibliothèque bibliothèque { get; private set; }

        public FenetreAccueil fenetre { get; set; }
        public RepertoireGroupe(FenetreAccueil fenetre,Bibliothèque bibliothèque)
        {
            InitializeComponent();
            this.bibliothèque = bibliothèque;
            StackBouton.DataContext = bibliothèque;
            this.fenetre = fenetre;
            ChangementDeMode();
        }
        private void Retour_Menu(object sender, RoutedEventArgs e)
        {
            fenetre.Show();
            fenetre.manager.Sauvegarde();
            this.Close();
        }

        private void appelStyleMusique(object sender, RoutedEventArgs e)
        {
            AjoutStyleMusique ajoutStyleMusique = new AjoutStyleMusique(bibliothèque);
            ajoutStyleMusique.Show();
        }
        private void AffichageElement_MouseDown(object sender, MouseButtonEventArgs e)
        {
            PresentationGroupe presentationGroupe = new PresentationGroupe((Groupe)Affichage.SelectedItem, this);
            presentationGroupe.Show();
            this.Hide();
        }
        private void NewElement(object sender, RoutedEventArgs e)
        {
            AjoutElement ajoutElement = new AjoutElement(bibliothèque);
            ajoutElement.Show();
        }

        private void Supprimer(object sender, MouseButtonEventArgs e)
        {
            Element element = (Element)Affichage.SelectedItem;
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir supprimer {element.Nom} ?", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                bibliothèque.SupprimerElement((Element)Affichage.SelectedItem);
                MessageBox.Show(this, $"Element Supprimé");
            }
        }
        private void SupprimerMusique(object sender, MouseButtonEventArgs e)
        {
            Musique musique = (Musique)StackBouton.SelectedItem;
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir supprimer {musique.Nom} ?\nTout les éléments de ce style vont être aussi supprimé", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                bibliothèque.LesMusiques.Remove(musique);
                MessageBox.Show(this, $"Style de Musique Supprimé");
            }
        }
        private void ChangementDeMode()
        {
            if (fenetre.Mode)
            {
                Affichage.Background = Brushes.White;
            }
            else
            {
                Affichage.Background = Brushes.Black;
                StackBouton.Background = Brushes.DarkBlue;
                Grille_Prsentation.Background = Brushes.DarkBlue;
            }

            if (fenetre.Daltonisme != 0)
            {
                var converter = new BrushConverter();
                if (fenetre.Daltonisme == 1)
                {
                    var brush = (Brush)converter.ConvertFromString("#FF9200A7");
                    Grille_Prsentation.Background = brush;
                    StackBouton.Background = brush;
                }
                else
                {
                    var brush = (Brush)converter.ConvertFromString("#FF00AD67");
                    Grille_Prsentation.Background = brush;
                    StackBouton.Background = brush;
                }
            }
        }
    }
}
