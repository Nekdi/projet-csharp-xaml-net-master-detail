﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour AjoutStyleMusique.xaml
    /// </summary>
    public partial class AjoutStyleMusique : Window
    {
        public Bibliothèque bibliothèque { get; private set; }
        public AjoutStyleMusique(Bibliothèque bibliothèque)
        {
            this.bibliothèque = bibliothèque;
            InitializeComponent();
        }

        private void Button_Valider(object sender, RoutedEventArgs e)
        {
            //Creation d'un style de musqiue avec le nom récupéré dans la TextBox
           Musique musique = new Musique(NomStyle.Text.ToString());
           bibliothèque.LesMusiques.Add(musique);
           this.Close();
        }

        private void Button_Refuser(object sender, RoutedEventArgs e)
        {
            // Annule la création d'un style de musique
            this.Close();
        }
    }
}
