﻿using Microsoft.Win32;
using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour RepertoireArtiste.xaml
    /// </summary>
    public partial class RepertoireArtiste : Window
    {
        public Bibliothèque bibliotheque { get; private set; }

       

        public FenetreAccueil fenetre { get; set; }
        public RepertoireArtiste (FenetreAccueil fenetre,Bibliothèque bibliothèque)
        {
            InitializeComponent();
            this.bibliotheque = bibliothèque;
            StackBouton.DataContext = bibliotheque;
            this.fenetre = fenetre;
            ChangementDeMode();
        }
        private void Retour_Menu(object sender, RoutedEventArgs e)
        {
            fenetre.Show();
            fenetre.manager.Sauvegarde();
            this.Close();
        }

        private void appelStyleMusique(object sender, RoutedEventArgs e)
        {
            AjoutStyleMusique ajoutStyleMusique = new AjoutStyleMusique(bibliotheque);
            ajoutStyleMusique.Show();
        }

        private void NewElement(object sender, RoutedEventArgs e)
        {
            AjoutElement ajoutElement = new AjoutElement(bibliotheque);
            ajoutElement.Show();
        }

        private void AffichageElement_MouseDown(object sender, MouseButtonEventArgs e)
        {
            PresentationArtiste presentationArtiste = new PresentationArtiste((Artiste)Affichage.SelectedItem,this);
            presentationArtiste.Show();
            this.Hide();
        }

        private void Supprimer(object sender, MouseButtonEventArgs e)
        {
            Element element = (Element)Affichage.SelectedItem;
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir supprimer {element.Nom} ?", "Confirmation", MessageBoxButton.YesNo);

           if(result == MessageBoxResult.Yes)
            {
                bibliotheque.SupprimerElement((Element)Affichage.SelectedItem);
                MessageBox.Show(this, $"Element Supprimé");
            }
        }

        private void SupprimerMusique(object sender, MouseButtonEventArgs e)
        {
            Musique musique = (Musique)StackBouton.SelectedItem;
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir supprimer {musique.Nom} ?\nTout les éléments de ce style vont être aussi supprimé", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                bibliotheque.LesMusiques.Remove(musique);
                MessageBox.Show(this, $"Style de Musique Supprimé");
            }
        }


        private void ChangementDeMode()
        {
            if (fenetre.Mode)
            {
                Affichage.Background = Brushes.White;
            }
            else
            {
                Affichage.Background = Brushes.Black;
                StackBouton.Background = Brushes.DarkRed;
                Grille_Prsentation.Background = Brushes.DarkRed;
            }

            if (fenetre.Daltonisme != 0)
            {
                var converter = new BrushConverter();
                if (fenetre.Daltonisme == 1)
                {
                    var brush = (Brush)converter.ConvertFromString("#FFEE002A");
                    Grille_Prsentation.Background = brush;
                    StackBouton.Background = brush;
                }
                else
                {
                    var brush = (Brush)converter.ConvertFromString("#FFA500A5");
                    Grille_Prsentation.Background = brush;
                    StackBouton.Background = brush;
                }
            }
        }

    }
}
