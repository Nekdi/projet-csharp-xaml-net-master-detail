﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour PresentationAssociation.xaml
    /// </summary>
    public partial class PresentationAssociation : Window
    {
        public Association association { get; private set; }

        public RepertoireAssociation repertoireAssociation { get; set; }
        public PresentationAssociation(Association association,RepertoireAssociation repertoireAssociation)
        {
            InitializeComponent();
            this.association = association;
            this.repertoireAssociation = repertoireAssociation;
            Presentation.DataContext = this.association;
            ChangementDeMode();
        }

        private void Retour_Menu(object sender, RoutedEventArgs e)
        {
            repertoireAssociation.fenetre.Show();
            repertoireAssociation.Close();
            this.Close();
        }

        private void RetourRepertoire(object sender, RoutedEventArgs e)
        {
            repertoireAssociation.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ListGroupe.Visibility = Visibility.Hidden;
            ListEvenement.Visibility = Visibility.Hidden;
            Grille_Presentation.Visibility = Visibility.Visible;
            Grille_Presentation_artiste.Visibility = Visibility.Hidden;
            AjoutInfo.Visibility = Visibility.Hidden;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ListGroupe.Visibility = Visibility.Hidden;
            ListEvenement.Visibility = Visibility.Hidden;
            Grille_Presentation.Visibility = Visibility.Hidden;
            Grille_Presentation_artiste.Visibility = Visibility.Visible;
            AjoutInfo.Visibility = Visibility.Visible;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ListGroupe.Visibility = Visibility.Visible;
            Grille_Presentation.Visibility = Visibility.Hidden;
            ListEvenement.Visibility = Visibility.Hidden;
            Grille_Presentation_artiste.Visibility = Visibility.Hidden;
            AjoutInfo.Visibility = Visibility.Visible;

        }
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ListGroupe.Visibility = Visibility.Hidden;
            Grille_Presentation.Visibility = Visibility.Hidden;
            ListEvenement.Visibility = Visibility.Visible;
            Grille_Presentation_artiste.Visibility = Visibility.Hidden;
            AjoutInfo.Visibility = Visibility.Visible;
        }
        private void RenseignerInformation(object sender, RoutedEventArgs e)
        {
            if (Grille_Presentation_artiste.Visibility == Visibility.Visible)
            {
                AjoutInformation ajoutInformation = new AjoutInformation(association, 1,repertoireAssociation.fenetre);
                ajoutInformation.Show();
            }
            if (ListGroupe.Visibility == Visibility.Visible)
            {
                AjoutInformation ajoutInformation = new AjoutInformation(association, 2, repertoireAssociation.fenetre);
                ajoutInformation.Show();
            }
            if (ListEvenement.Visibility == Visibility.Visible)
            {
                AjoutInformation ajoutInformation = new AjoutInformation(association, 4, repertoireAssociation.fenetre);
                ajoutInformation.Show();
            }
            repertoireAssociation.Show();
            this.Close();
        }

        private void Modificiation(object sender, RoutedEventArgs e)
        {
            ModificationElement modification = new ModificationElement(association);
            modification.Show();
        }
        private void ChangementDeMode()
        {

            if (repertoireAssociation.fenetre.Mode)
            {
                GrilleFond.Background = Brushes.White;
            }
            else {
                GrilleFond.Background = Brushes.Black;
                GrilleCouleur.Background = Brushes.DarkSlateBlue;
            }

            if (repertoireAssociation.fenetre.Daltonisme != 0)
            {
                var converter = new BrushConverter();
                if (repertoireAssociation.fenetre.Daltonisme == 1)
                {
                    var brush = (Brush)converter.ConvertFromString("#FF008D9B");
                    GrilleCouleur.Background = brush;
                }
                else
                {
                    var brush = (Brush)converter.ConvertFromString("#FFFEA900");
                    GrilleCouleur.Background = brush;
                }
            }
        }

    }
}
