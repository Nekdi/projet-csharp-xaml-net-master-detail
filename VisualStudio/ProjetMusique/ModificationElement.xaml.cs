﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour ModificationElement.xaml
    /// </summary>
    public partial class ModificationElement : Window
    {
        public Element element { get; set; }
        public ModificationElement(Element element)
        {
            InitializeComponent();
            this.element = element;
            GrilleModification.DataContext = this.element;
        }

        /// <summary>
        /// Supprime le réseaux sélectionné dans la liste en affcichant une messagebox de confirmation avant suppression définitive
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SupprimerReseau(object sender, RoutedEventArgs e)
        {
            Reseaux reseaux = (Reseaux)ListReseau.SelectedItem;
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir supprimer {reseaux.Nom} ?", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                element.LesReseaux.Remove((Reseaux)ListReseau.SelectedItem);
                MessageBox.Show(this, $"Element Supprimé");
            }
        }


        /// <summary>
        /// Demande à l'utilisateur via une messagebox si il souhaite réellment modifier le réseau, si il accepte, le réseau et supprimé
        /// et les anciennes informations sont anvoyés dans des TextBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModifierReseau(object sender, RoutedEventArgs e)
        {
            Reseaux reseaux = (Reseaux)ListReseau.SelectedItem;
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir modifier {reseaux.Nom} ?\nLa Modification supprimera l'ancien reseau.", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                TextBoxNom.Text = reseaux.Nom;
                TextBoxLien.Text = reseaux.Lien;
                element.LesReseaux.Remove(reseaux);
            }
        }

        /// <summary>
        /// Ajoute un réseau à la liste Element.LesRéseaux de l'éléments envoyé dans la page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AjoutReseau(object sender, RoutedEventArgs e)
        {
            Reseaux reseaux = new Reseaux(TextBoxNom.Text.ToString(),TextBoxLien.Text.ToString());
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir Ajouter {reseaux.Nom} à {element.Nom}", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                element.LesReseaux.Add(reseaux);
                TextBoxLien.Clear();
                TextBoxNom.Clear();
            }
        }
       

        private void Clear(object sender, RoutedEventArgs e)
        {
            TextBoxLien.Clear();
            TextBoxNom.Clear();
        }

        private void Retour(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show(this, $"Voulez vous garder ces ajouts pour {element.Nom} ?", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                this.Close();
            }
        }
    }
}
