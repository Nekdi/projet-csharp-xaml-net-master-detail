﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour RepertoireEvenement.xaml
    /// </summary>
    public partial class RepertoireEvenement : Window
    {
        public Bibliothèque bibliothèque { get; private set; }

        

        public FenetreAccueil fenetre { get; set; }
        public RepertoireEvenement(FenetreAccueil fenetre, Bibliothèque bibliothèque)
        {
            InitializeComponent();
            this.fenetre = fenetre;
            this.bibliothèque = bibliothèque;
            StackBouton.DataContext = bibliothèque;
            ChangementDeMode();
        }

        private void AffichageElement_MouseDown(object sender, MouseButtonEventArgs e)
        {
            PresentationEvenement evenement = new PresentationEvenement((Evenement)Affichage.SelectedItem, this);
            evenement.Show();
            this.Hide();
        }

        private void Retour_Menu(object sender, RoutedEventArgs e)
        {
            fenetre.Show();
            fenetre.manager.Sauvegarde();
            this.Close();
        }

        private void appelStyleMusique(object sender, RoutedEventArgs e)
        {
            AjoutStyleMusique ajoutStyleMusique = new AjoutStyleMusique(bibliothèque);
            ajoutStyleMusique.Show();
        }

        private void NewElement(object sender, RoutedEventArgs e)
        {
            AjoutElement ajoutElement = new AjoutElement(bibliothèque);
            ajoutElement.Show();
        }

        private void Supprimer(object sender, MouseButtonEventArgs e)
        {
            Element element = (Element)Affichage.SelectedItem;
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir supprimer {element.Nom} ?", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                bibliothèque.SupprimerElement((Element)Affichage.SelectedItem);
                MessageBox.Show(this, $"Element Supprimé");
            }
        }

        private void SupprimerMusique(object sender, MouseButtonEventArgs e)
        {
            Musique musique = (Musique)StackBouton.SelectedItem;
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir supprimer {musique.Nom} ?\nTout les éléments de ce style vont être aussi supprimé", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                bibliothèque.LesMusiques.Remove(musique);
                MessageBox.Show(this, $"Style de Musique Supprimé");
            }
        }
        private void ChangementDeMode()
        {
            if (fenetre.Mode)
            {
                Affichage.Background = Brushes.White;
            }
            else
            {
                Affichage.Background = Brushes.Black;
                Grille_Presentation.Background = Brushes.DarkGreen;
                BoutonHome.Background = Brushes.DarkGreen;
                BoutonPlus.Background = Brushes.DarkGreen;
            }

            if (fenetre.Daltonisme != 0)
            {
                var converter = new BrushConverter();
                if (fenetre.Daltonisme == 1)
                {
                    var brush = (Brush)converter.ConvertFromString("#FF9EED00");
                    Grille_Presentation.Background = brush;
                    BoutonPlus.Background = brush;
                    BoutonHome.Background = brush;
                }
                else
                {
                    var brush = (Brush)converter.ConvertFromString("#FF2200AF");
                    Grille_Presentation.Background = brush;
                    BoutonPlus.Background = brush;
                    BoutonHome.Background = brush;
                }
            }
        }
    }
}
