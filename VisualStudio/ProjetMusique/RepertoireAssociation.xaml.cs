﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour RepertoireAssociation.xaml
    /// </summary>
    public partial class RepertoireAssociation : Window
    {
        public Bibliothèque bibliothèque { get; private set; }

        
        public FenetreAccueil fenetre { get; set; }

        public RepertoireAssociation(FenetreAccueil fenetre, Bibliothèque bibliothèque)
        {
            InitializeComponent();
            this.bibliothèque = bibliothèque;
            this.fenetre = fenetre;
            StackBouton.DataContext = bibliothèque;
            ChangementDeMode();
        }

        private void AffichageElement_MouseDown(object sender, MouseButtonEventArgs e)
        {
            PresentationAssociation presentationAssociation = new PresentationAssociation((Association)Affichage.SelectedItem, this);
            presentationAssociation.Show();
            this.Hide();
        }

        private void Retour_Menu(object sender, RoutedEventArgs e)
        {
            fenetre.Show();
            fenetre.manager.Sauvegarde();
            this.Close();
        }

        private void appelStyleMusique(object sender, RoutedEventArgs e)
        {
            AjoutStyleMusique ajoutStyleMusique = new AjoutStyleMusique(bibliothèque);
            ajoutStyleMusique.Show();
        }

        private void NewElement(object sender, RoutedEventArgs e)
        {
            AjoutElement ajoutElement = new AjoutElement(bibliothèque);
            ajoutElement.Show();
        }

        private void Supprimer(object sender, MouseButtonEventArgs e)
        {
            Element element = (Element)Affichage.SelectedItem;
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir supprimer {element.Nom} ?", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                bibliothèque.SupprimerElement((Element)Affichage.SelectedItem);
                MessageBox.Show(this, $"Element Supprimé");
            }
        }
        private void SupprimerMusique(object sender, MouseButtonEventArgs e)
        {
            Musique musique = (Musique)StackBouton.SelectedItem;
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir supprimer {musique.Nom} ?\nTout les éléments de ce style vont être aussi supprimé", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                bibliothèque.LesMusiques.Remove(musique);
                MessageBox.Show(this, $"Style de Musique Supprimé");
            }
        }
        private void ChangementDeMode()
        {
            if (fenetre.Mode)
            {
                Affichage.Background = Brushes.White;
            }
            else 
            { 
                Affichage.Background = Brushes.Black;
                Grille_Presentation.Background = Brushes.DarkSlateBlue;
                BoutonHome.Background = Brushes.DarkSlateBlue;
                BoutonPlus.Background = Brushes.DarkSlateBlue;
            }


            if (fenetre.Daltonisme != 0)
            {
                var converter = new BrushConverter();
                if (fenetre.Daltonisme == 1)
                {
                    var brush = (Brush)converter.ConvertFromString("#FF008D9B");
                    Grille_Presentation.Background = brush;
                    BoutonHome.Background = brush;
                    BoutonPlus.Background = brush;
                }
                else
                {
                    var brush = (Brush)converter.ConvertFromString("#FFFEA900");
                    Grille_Presentation.Background = brush;
                    BoutonHome.Background = brush;
                    BoutonPlus.Background = brush;
                }
            }
        }
    }
}
