﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour UCPresentation.xaml
    /// </summary>
    public partial class UCPresentation : UserControl
    {
        public UCPresentation()
        {
            InitializeComponent();
        }

        private void Ouvrir_Reseaux(object sender, MouseButtonEventArgs e)
        {
            if(ListReseaux.SelectedItem != null)
            {
                Reseaux reseaux = (Reseaux)ListReseaux.SelectedItem;
                Process.Start("cmd", "/c start" + " " + reseaux.Lien);
            }
                   
           
        }
    }
}
