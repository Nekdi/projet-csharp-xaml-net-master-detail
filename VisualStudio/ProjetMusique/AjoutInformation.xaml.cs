﻿using Microsoft.Win32;
using Model;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour AjoutInformation.xaml
    /// </summary>
    public partial class AjoutInformation : Window
    {
        public Element element { get; private set; }

        public int Code { get; private set; }

        public FenetreAccueil fenetre { get; set; }
        public AjoutInformation(Element ElementMAJ, int CodeAjout,FenetreAccueil fenetre)
        {
            InitializeComponent();
            element = ElementMAJ;
            Code = CodeAjout;
            this.fenetre = fenetre;
            AfficheNom.DataContext = element;
            MiseEnPlace();
            
        }

        private void MiseEnPlace()
        {
            /* Mise en place du data context */

            if(Code == 1)
            {
                ListStyle.DataContext = fenetre.manager.LesBibliotheques[0];
            }
            if (Code == 2)
            {
                ListStyle.DataContext = fenetre.manager.LesBibliotheques[1];
            }
            if (Code == 3)
            {
                ListStyle.DataContext = fenetre.manager.LesBibliotheques[2];
            }
            if (Code ==4)
            {
                ListStyle.DataContext = fenetre.manager.LesBibliotheques[3];
            }


        }

        private void AffichageElement_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (Affichage.SelectedItem != null)
            {
                if (Code == 1)
                {
                    Artiste artiste = (Artiste)Affichage.SelectedItem;
                    if (element.SaBibliotheque.identifiant == 2)
                    {
                        Groupe groupe = (Groupe)element;
                        if (groupe.SesArtistes.Contains(artiste))
                        {
                            MessageBox.Show($"{groupe.Nom} possède déjà {artiste.Nom}.\nRetour.");
                            this.Close();
                        }
                        if(artiste.SonGroupe != null)
                        {
                            artiste.SonGroupe.SesArtistes.Remove(artiste);
                        }
                        groupe.SetArtiste(artiste);
                        artiste.SetGroupe(groupe);
                    }
                    if (element.SaBibliotheque.identifiant == 3)
                    {
                        Association association = (Association)element;
                        if (association.SesArtistes.Contains(artiste))
                        {
                            MessageBox.Show($"{association.Nom} possède déjà {artiste.Nom}.\nRetour.");
                            this.Close();
                        }
                        association.SetArtiste(artiste);
                        artiste.SetAssociation(association);
                    }
                    if (element.SaBibliotheque.identifiant == 4)
                    {
                        Evenement evenement = (Evenement)element;
                        if (evenement.SesArtistes.Contains(artiste))
                        {
                            MessageBox.Show($"{evenement.Nom} possède déjà {artiste.Nom}.\nRetour.");
                            this.Close();
                        }
                        evenement.SetArtiste(artiste);
                        artiste.SetEvenement(evenement);
                    }
                }
                if (Code == 2)
                {
                   Groupe groupe = (Groupe)Affichage.SelectedItem;
                    if(element.SaBibliotheque.identifiant == 1)
                    {
                        Artiste artiste = (Artiste)element;
                        if (artiste.SonGroupe != null)
                        {
                            artiste.SonGroupe.SesArtistes.Remove(artiste);
                        }
                        groupe.SetArtiste(artiste);
                        artiste.SetGroupe(groupe);
                    }
                    if (element.SaBibliotheque.identifiant == 3)
                    {
                        Association association = (Association)element;
                        if (association.SesGroupes.Contains(groupe))
                        {
                            MessageBox.Show($"{association.Nom} possède déjà {groupe.Nom}.\nRetour.");
                            this.Close();
                        }
                        association.SetGroupe(groupe);
                        groupe.SetAssociation(association);
                    }
                    if (element.SaBibliotheque.identifiant == 4)
                    {
                        Evenement evenement = (Evenement)element;
                        if (evenement.SesGroupes.Contains(groupe))
                        {
                            MessageBox.Show($"{evenement.Nom} possède déjà {groupe.Nom}.\nRetour.");
                            this.Close();
                        }
                        evenement.SetGroupe(groupe);
                        groupe.SetEvenement(evenement);
                    }
                }
                if (Code == 3)
                {
                    Association association = (Association)Affichage.SelectedItem;
                    if (element.SaBibliotheque.identifiant == 2)
                    {
                        Groupe groupe = (Groupe)element;
                        if (groupe.SesAssociations.Contains(association))
                        {
                            MessageBox.Show($"{groupe.Nom} possède déjà {association.Nom}.\nRetour.");
                            this.Close();
                        }
                        groupe.SetAssociation(association);
                        association.SetGroupe(groupe);
                    }
                    if (element.SaBibliotheque.identifiant == 1)
                    {
                        Artiste artiste = (Artiste)element;
                        if (artiste.SesAssociations.Contains(association))
                        {
                            MessageBox.Show($"{artiste.Nom} possède déjà {association.Nom}.\nRetour.");
                            this.Close();
                        }
                        association.SetArtiste(artiste);
                        artiste.SetAssociation(association);
                    }
                    if (element.SaBibliotheque.identifiant == 4)
                    {
                        Evenement evenement = (Evenement)element;
                        if (evenement.SesAssociations.Contains(association))
                        {
                            MessageBox.Show($"{evenement.Nom} possède déjà {association.Nom}.\nRetour.");
                            this.Close();
                        }
                        evenement.SetAssociation(association);
                        association.SetEvenement(evenement);
                    }
                }
                if (Code == 4)
                {
                   Evenement evenement = (Evenement)Affichage.SelectedItem;
                    if (element.SaBibliotheque.identifiant == 2)
                    {
                        Groupe groupe = (Groupe)element;
                        if (groupe.SesEvenements.Contains(evenement))
                        {
                            MessageBox.Show($"{groupe.Nom} possède déjà {evenement.Nom}.\nRetour.");
                            this.Close();
                        }
                        groupe.SetEvenement(evenement);
                        evenement.SetGroupe(groupe);
                    }
                    if (element.SaBibliotheque.identifiant == 3)
                    {
                        Association association = (Association)element;
                        if (association.SesEvenements.Contains(evenement))
                        {
                            MessageBox.Show($"{association.Nom} possède déjà {evenement.Nom}.\nRetour.");
                            this.Close();
                        }
                        association.SetEvenement(evenement);
                        evenement.SetAssociation(association);
                    }
                    if (element.SaBibliotheque.identifiant == 1)
                    {
                        Artiste artiste = (Artiste)element;
                        if (artiste.SesEvenements.Contains(evenement))
                        {
                            MessageBox.Show($"{artiste.Nom} possède déjà {evenement.Nom}.\nRetour.");
                            this.Close();
                        }
                        evenement.SetArtiste(artiste);
                        artiste.SetEvenement(evenement);
                    }
                }
            }
            this.Close();
        }
    }
}
