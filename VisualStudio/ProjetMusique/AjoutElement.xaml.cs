﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour AjoutElement.xaml
    /// </summary>
    public partial class AjoutElement : Window
    {
        public Bibliothèque bibliothèque { get; private set; }

        /// <summary>
        /// Constructeur de la page
        /// </summary>
        /// <param name="bibliothèque"> bibliotheque dans laquelle l'élément va être ajouté </param>
        public AjoutElement(Bibliothèque bibliothèque)
        {
            InitializeComponent();
            this.bibliothèque = bibliothèque;
            ListMusique.DataContext = this.bibliothèque;
        }

        /// <summary>
        ///  Fonction dans le cas où l'utilisteur annule la création de l'élément
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir annuler cette création ?", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                MessageBox.Show("Creation Annuler");
            }
            this.Close();
        }

        /// <summary>
        /// Fonction dans le cas où l'utilisateur créer un élément
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {            
           int i = bibliothèque.LesMusiques.IndexOf((Musique)MusiqueElement.DataContext);
            var result = MessageBox.Show(this, $"Etes vous sur de vouloir creer cet element\nLa Description,Le Nom et l'image ne pourront être modifiés", "Confirmation", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                // Regarde pour chaque identifiant de biliotheque entre 1 et 4 pour créer un element fille à placer dans la bibliotheque
                if (bibliothèque.identifiant == 1)
                {
                    bibliothèque.LesMusiques[i].LesElements.Add(new Artiste(DescriptionElement.Text.ToString(), NomElement.Text.ToString(), (Musique)MusiqueElement.DataContext, ImageElement.Text.ToString(), bibliothèque));
                }
                if (bibliothèque.identifiant == 2)
                {
                    bibliothèque.LesMusiques[i].LesElements.Add(new Groupe(DescriptionElement.Text.ToString(), NomElement.Text.ToString(), (Musique)MusiqueElement.DataContext, ImageElement.Text.ToString(), bibliothèque));
                }
                if (bibliothèque.identifiant == 3)
                {
                    bibliothèque.LesMusiques[i].LesElements.Add(new Association(DescriptionElement.Text.ToString(), NomElement.Text.ToString(), (Musique)MusiqueElement.DataContext, ImageElement.Text.ToString(), bibliothèque));
                }
                if (bibliothèque.identifiant == 4)
                {
                    bibliothèque.LesMusiques[i].LesElements.Add(new Evenement(DescriptionElement.Text.ToString(), NomElement.Text.ToString(), (Musique)MusiqueElement.DataContext, ImageElement.Text.ToString(), bibliothèque));
                }
            }            
            this.Close();
        }
    }
}
