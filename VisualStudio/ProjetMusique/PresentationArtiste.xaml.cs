﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour PresentationArtiste.xaml
    /// </summary>
    public partial class PresentationArtiste : Window
    {
        public Artiste artiste { get; private set; }

        public RepertoireArtiste RepertoireArtiste { get; private set; }
        public PresentationArtiste( Artiste artiste, RepertoireArtiste repertoire)
        {
            InitializeComponent();
            RepertoireArtiste = repertoire;
            this.artiste = artiste;
            Presentation.DataContext = artiste;
            ChangementDeMode();
            Grille_Presentation_Groupe.DataContext = artiste.SonGroupe;
        }
        private void Retour_Menu(object sender, RoutedEventArgs e)
        {
            RepertoireArtiste.fenetre.Show();
            RepertoireArtiste.Close();
            this.Close();
        }

        private void RetourRepertoire(object sender, RoutedEventArgs e)
        {
            RepertoireArtiste.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ListAssociation.Visibility = Visibility.Hidden;
            ListEvenement.Visibility = Visibility.Hidden;
            Grille_Presentation.Visibility = Visibility.Visible;
            Grille_Presentation_Groupe.Visibility = Visibility.Hidden;
            AjoutInfo.Visibility = Visibility.Hidden;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ListAssociation.Visibility = Visibility.Hidden;
            ListEvenement.Visibility = Visibility.Hidden;
            Grille_Presentation.Visibility = Visibility.Hidden;
            Grille_Presentation_Groupe.Visibility = Visibility.Visible;
            if(artiste.SonGroupe == null)
            {
                AjoutInfo.Visibility = Visibility.Visible;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ListAssociation.Visibility = Visibility.Visible;
            Grille_Presentation.Visibility = Visibility.Hidden;
            ListEvenement.Visibility = Visibility.Hidden;
            Grille_Presentation_Groupe.Visibility = Visibility.Hidden;
            AjoutInfo.Visibility = Visibility.Visible;

        }
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ListAssociation.Visibility = Visibility.Hidden;
            Grille_Presentation.Visibility = Visibility.Hidden;
            ListEvenement.Visibility = Visibility.Visible;
            Grille_Presentation_Groupe.Visibility = Visibility.Hidden;
            AjoutInfo.Visibility = Visibility.Visible;
        }
        private void RenseignerInformation(object sender, RoutedEventArgs e)
        {
           if(Grille_Presentation_Groupe.Visibility == Visibility.Visible)
            {
                AjoutInformation ajoutInformation = new AjoutInformation(artiste, 2,RepertoireArtiste.fenetre);
                ajoutInformation.Show();
            }
           if(ListAssociation.Visibility == Visibility.Visible)
            {
                AjoutInformation ajoutInformation = new AjoutInformation(artiste, 3, RepertoireArtiste.fenetre);
                ajoutInformation.Show();
            }
            if (ListEvenement.Visibility == Visibility.Visible)
            {
                AjoutInformation ajoutInformation = new AjoutInformation(artiste, 4, RepertoireArtiste.fenetre);
                ajoutInformation.Show();
            }
            RepertoireArtiste.Show();
            this.Close();
        }

        private void Modificiation(object sender, RoutedEventArgs e)
        {
            ModificationElement modification = new ModificationElement(artiste);
            modification.Show();
        }
        private void ChangementDeMode()
        {
            if (RepertoireArtiste.fenetre.Mode)
            {
                GrilleFond.Background = Brushes.White;
            }
            else
            {
                GrilleFond.Background = Brushes.Black;
                GrilleCouleur.Background = Brushes.DarkRed;
            }

            if(RepertoireArtiste.fenetre.Daltonisme != 0)
            {
                var converter = new BrushConverter();
                if(RepertoireArtiste.fenetre.Daltonisme == 1)
                {
                    var brush = (Brush)converter.ConvertFromString("#FFEE002A");
                    GrilleCouleur.Background = brush;
                }
                else
                {
                    var brush = (Brush)converter.ConvertFromString("#FFA500A5");
                    GrilleCouleur.Background = brush;
                }
            }
        }
    }
}
