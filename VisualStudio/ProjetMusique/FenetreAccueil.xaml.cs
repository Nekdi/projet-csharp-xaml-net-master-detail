﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Text.Encodings.Web;
using System.Windows;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour FenetreAccueil.xaml
    /// </summary>
    public partial class FenetreAccueil : Window
    {
        public Manager manager { get; private set; }

        public bool Mode { get; set; }

        public int Daltonisme { get; set; }
        public FenetreAccueil()
        {
            InitializeComponent();
            (Application.Current as App).manager.Chargement();
            manager = (Application.Current as App).manager;
            Mode = true;
        }

        private void Ouvrir_Repertoire_Artiste(object sender, RoutedEventArgs e)
        {
            RepertoireArtiste repertoire = new RepertoireArtiste(this,manager.LesBibliotheques[0]);
            repertoire.Show();
            this.Hide();
        }

        private void Ouvrir_Repertoire_Groupe(object sender, RoutedEventArgs e)
        {
            RepertoireGroupe repertoire = new RepertoireGroupe(this, manager.LesBibliotheques[1]);
            repertoire.Show();
            this.Hide();

        }

        private void Ouvrir_Repertoire_Evenement(object sender, RoutedEventArgs e)
        {
            RepertoireEvenement repertoire = new RepertoireEvenement(this, manager.LesBibliotheques[2]);
            repertoire.Show();
            this.Hide();

        }

        private void Ouvrir_Repertoire_Association(object sender, RoutedEventArgs e)
        {
            RepertoireAssociation repertoire = new RepertoireAssociation(this, manager.LesBibliotheques[3]);
            repertoire.Show();
            this.Hide();
        }

        /// <summary>
        /// Changement de mode de l'application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangementDeMode(object sender, RoutedEventArgs e)
        {
            if(Daltonisme != 0)
            {
                if (Mode)
                {
                    Mode = false;
                    BoutonMode.Content = "Nuit";
                    BoutonMode.Background = Brushes.Silver;
                    GrillePrincipale.Background = Brushes.Black;
                }
                else
                {
                    Mode = true;
                    BoutonMode.Content = "Jour";
                    BoutonMode.Background = Brushes.DeepSkyBlue;
                    GrillePrincipale.Background = Brushes.White;
                }
            }
            else
            {
                if (Mode)
                {
                    Mode = false;
                }
                else Mode = true;
                SetVision();
            }
        }

        /// <summary>
        /// Met en place une vision de nuit ou non en Fonction de FentreAccueil.Mode
        /// </summary>
        private void SetVision()
        {
            if (Mode)
            {
                ModeJour();
            }
            else ModeNuit();
        }

        /// <summary>
        /// Met en place le mode jour de l'application avec les couleurs de base
        /// </summary>
        private void ModeJour()
        {
            BoutonMode.Content = "Jour";
            BoutonMode.Background = Brushes.DeepSkyBlue;
            repertoire_Artiste_bouton.Background = Brushes.Tomato;
            repertoire_Groupe_bouton.Background = Brushes.DodgerBlue;
            repertoire_Evenement_bouton.Background = Brushes.SeaGreen;
            repertoire_Association_bouton.Background = Brushes.MediumPurple;
            GrillePrincipale.Background = Brushes.White;
        }

        /// <summary>
        /// Met en place le mode Nuit de l'application avec des couleurs plus sombre
        /// </summary>
        private void ModeNuit()
        {
            BoutonMode.Content = "Nuit";
            BoutonMode.Background = Brushes.Silver;
            repertoire_Artiste_bouton.Background = Brushes.DarkRed;
            repertoire_Groupe_bouton.Background = Brushes.DarkBlue;
            repertoire_Evenement_bouton.Background = Brushes.DarkGreen;
            repertoire_Association_bouton.Background = Brushes.DarkSlateBlue;
            var converter = new BrushConverter();
            var brush = (Brush)converter.ConvertFromString("#FF000000");
            GrillePrincipale.Background = Brushes.Black;
        }

        /// <summary>
        /// Change le mode Daltonisme parmi les trois types défini dans l'application par rapport à FenetreAccueil.Daltonisme
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangementDaltonien(object sender, RoutedEventArgs e)
        {
            Daltonisme++;
            Daltonisme = Daltonisme % 3;
            if (Daltonisme == 0)
            {
                VisionNormal();
            }
            if (Daltonisme == 1)
            {
                VisionDeuteranope();
            }
            if(Daltonisme == 2)
            {
                VisionTritanope();
            }
        }


        /// <summary>
        /// Met en place le Mode "Vision Normale"
        /// </summary>
        private void VisionNormal()
        {
            BoutonDaltonien.Content = "Vision Normale";
            SetVision();
        }


        /// <summary>
        /// Met en place le Mode "VisionDeuteranope" avec les bonnes couleurs
        /// </summary>
        private void VisionDeuteranope()
        {
            BoutonDaltonien.Content = "Vision Deuteranope";
            var converter = new BrushConverter();
            var brush = (Brush)converter.ConvertFromString("#FFEE002A");
            repertoire_Artiste_bouton.Background = brush;
             brush = (Brush)converter.ConvertFromString("#FF9200A7");
            repertoire_Groupe_bouton.Background = brush;
             brush = (Brush)converter.ConvertFromString("#FF9EED00");
            repertoire_Evenement_bouton.Background = brush;
             brush = (Brush)converter.ConvertFromString("#FF008D9B");
            repertoire_Association_bouton.Background = brush;
        }

        /// <summary>
        /// Met en place le Mode "VisionTritanope" avec les bonnes couleurs
        /// </summary>
        private void VisionTritanope()
        {
            BoutonDaltonien.Content = "Vision Tritanope";
            var converter = new BrushConverter();
            var brush = (Brush)converter.ConvertFromString("#FFA500A5");
            repertoire_Artiste_bouton.Background = brush;
            brush = (Brush)converter.ConvertFromString("#FF00AD67");
            repertoire_Groupe_bouton.Background = brush;
            brush = (Brush)converter.ConvertFromString("#FF2200AF");
            repertoire_Evenement_bouton.Background = brush;
            brush = (Brush)converter.ConvertFromString("#FFFEA900");
            repertoire_Association_bouton.Background = brush;
        }



    }
}
