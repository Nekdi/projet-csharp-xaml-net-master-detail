﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Model;

namespace ProjetMusique
{
    /// <summary>
    /// Logique d'interaction pour PresentationEvenement.xaml
    /// </summary>
    public partial class PresentationEvenement : Window
    {
        public Evenement evenement { get; set; }

        public RepertoireEvenement repertoireEvenement { get; set; }
        public PresentationEvenement(Evenement evenement,RepertoireEvenement repertoireEvenement)
        {
            InitializeComponent();
            this.evenement = evenement;
            this.repertoireEvenement = repertoireEvenement;
            Presentation.DataContext = this.evenement;
            ChangementDeMode();
        }

        private void Retour_Menu(object sender, RoutedEventArgs e)
        {
            repertoireEvenement.fenetre.Show();
            repertoireEvenement.Close();
            this.Close();
        }

        private void RetourRepertoire(object sender, RoutedEventArgs e)
        {
            repertoireEvenement.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ListAssociation.Visibility = Visibility.Hidden;
            ListGroupe.Visibility = Visibility.Hidden;
            Grille_Presentation.Visibility = Visibility.Visible;
            Grille_Presentation_artiste.Visibility = Visibility.Hidden;
            AjoutInfo.Visibility = Visibility.Hidden;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ListAssociation.Visibility = Visibility.Hidden;
            ListGroupe.Visibility = Visibility.Hidden;
            Grille_Presentation.Visibility = Visibility.Hidden;
            Grille_Presentation_artiste.Visibility = Visibility.Visible;
            AjoutInfo.Visibility = Visibility.Visible;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ListAssociation.Visibility = Visibility.Visible;
            Grille_Presentation.Visibility = Visibility.Hidden;
            ListGroupe.Visibility = Visibility.Hidden;
            Grille_Presentation_artiste.Visibility = Visibility.Hidden;
            AjoutInfo.Visibility = Visibility.Visible;

        }
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ListAssociation.Visibility = Visibility.Hidden;
            Grille_Presentation.Visibility = Visibility.Hidden;
            ListGroupe.Visibility = Visibility.Visible;
            Grille_Presentation_artiste.Visibility = Visibility.Hidden;
            AjoutInfo.Visibility = Visibility.Visible;
        }
        private void RenseignerInformation(object sender, RoutedEventArgs e)
        {
            if (Grille_Presentation_artiste.Visibility == Visibility.Visible)
            {
                AjoutInformation ajoutInformation = new AjoutInformation(evenement, 1,repertoireEvenement.fenetre);
                ajoutInformation.Show();
            }
            if (ListAssociation.Visibility == Visibility.Visible)
            {
                AjoutInformation ajoutInformation = new AjoutInformation(evenement, 3, repertoireEvenement.fenetre);
                ajoutInformation.Show();
            }
            if (ListGroupe.Visibility == Visibility.Visible)
            {
                AjoutInformation ajoutInformation = new AjoutInformation(evenement, 2, repertoireEvenement.fenetre);
                ajoutInformation.Show();
            }
            repertoireEvenement.Show();
            this.Close();
        }

        private void Modificiation(object sender, RoutedEventArgs e)
        {
            ModificationElement modification = new ModificationElement(evenement);
            modification.Show();
        }
        private void ChangementDeMode()
        {
            if (repertoireEvenement.fenetre.Mode)
            {
                GrilleCouleur.Background = Brushes.White;
            }
            else
            {
                GrilleCouleur.Background = Brushes.Black;
                GrilleCouleur.Background = Brushes.DarkGreen;
            }

            if (repertoireEvenement.fenetre.Daltonisme != 0)
            {
                var converter = new BrushConverter();
                if (repertoireEvenement.fenetre.Daltonisme == 1)
                {
                    var brush = (Brush)converter.ConvertFromString("#FF9EED00");
                    GrilleFond.Background = brush;
                }
                else
                {
                    var brush = (Brush)converter.ConvertFromString("#FF2200AF");
                    GrilleFond.Background = brush;
                }
            }
        }

       

    }
}
