﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Microsoft.Win32.SafeHandles;
using Model;

namespace Stub
{
    public class Stub
    {
        /// <summary>
        /// Création d'un jeu d'essai pour l'application
        /// </summary>
        /// <returns></returns>
        public static Manager Data()
        {
            Manager manager = new Manager();
            manager.LesBibliotheques.Add(CreerBibliothequeArtiste());
            manager.LesBibliotheques.Add(CreerBibliothequeGroupe());
            manager.LesBibliotheques.Add(CreerBibliothequeEvenement());
            manager.LesBibliotheques.Add(CreerBibliothequeAssociation());
            return manager;
        }


        public static Bibliothèque CreerBibliothequeArtiste()
        {
            Bibliothèque bibliothèque = new Bibliothèque(1);
            Musique musique1 = new Musique("Rap");
            Musique musique2 = new Musique("EDM");
            Musique musique3 = new Musique("Rock");

            bibliothèque.AjouterElement(new Artiste("Enfant terrible des années 90, Kikesa s'impose comme un condensé de pop culture et de musique, catalysant ses influences dans un son, un flow et une attitude unique : le nouveau hippie.\nLe jeune artiste remet ces idiomes au gout du jour en y apportant la fraicheur et la verve du hip-hop (scène dans laquelle il excelle depuis plus de 10 ans), sans oublier un sens aigu de l'autodérision, propre à sa génération.","KIKESA",musique1,"https://images-na.ssl-images-amazon.com/images/I/81lBlONgq-L._SL1200_.jpg", bibliothèque));
            bibliothèque.AjouterElement(new Artiste("Il publie son premier single, Sort le cross volé, en novembre 2013 suivi en février 2014 d'un album entier, Dans ma paranoïa, le premier d'une série prolifique : deux albums complets par an depuis le début de sa carrière, tous certifiés au moins disque de platine.\nEn 2015, Jul quitte le label Liga One Industry à la suite de désaccords financiers et fonde son propre label indépendant, D'or et de platine. L'année suivante, il reçoit la récompense du meilleur album de musique urbaine aux 32e Victoires de la musique pour l'album My World. ", "JUL", musique1, "https://images.genius.com/11e7df0856f7dcb3ba55c4bfb724444b.640x640x1.jpg", bibliothèque));
            bibliothèque.AjouterElement(new Artiste("Originaire de Sydney, Timmy Trumpet débute en 2005 et est supporté par de nombreux artistes (dont Jacques Joubaud et Nathan Nicol) et groupes mondialement connus comme la Swedish House Mafia, qu'il accompagnera lors de sa tournée en Australie en 2012. ", "Timmy Trumpet", musique2,"https://upload.wikimedia.org/wikipedia/commons/9/9a/Portrait_Timmy_Trumpet.jpg", bibliothèque));
            bibliothèque.AjouterElement(new Artiste("Freddie Mercury, né Farrokh Bulsara le 5 septembre 1946 à Stone Town, dans le protectorat de Zanzibar, et mort le 24 novembre 1991 à Londres, est un auteur-compositeur-interprète et musicien britannique, cofondateur en 1970 et chanteur-pianiste du groupe de rock Queen, au sein duquel il a établi sa réputation internationale, en compagnie du guitariste Brian May, du batteur Roger Taylor et du bassiste John Deacon, tous auteurs-compositeurs comme lui.", "Freddy Mercury", musique3, "https://resize-parismatch.lanmedia.fr/img/var/news/storage/images/paris-match/people-a-z/freddie-mercury/6136436-6-fre-FR/Freddie-Mercury.jpg", bibliothèque));

            return bibliothèque;
        }
       public static Bibliothèque CreerBibliothequeAssociation()
        {
            Bibliothèque bibliothèque = new Bibliothèque(3);
            Musique musique1 = new Musique("Variété Française");
            bibliothèque.AjouterElement(new Association("Fondés par Coluche en 1985, les Restos du Cœur est une association loi de 1901, reconnue d’utilité publique, sous le nom officiel de « les Restaurants du Cœur – les Relais du Cœur ». Ils ont pour but « d’aider et d’apporter une assistance bénévole aux personnes démunies, notamment dans le domaine alimentaire par l’accès à des repas gratuits, et par la participation à leur insertion sociale et économique, ainsi qu’à toute action contre la pauvreté sous toutes ses formes","Les Resto du Coeur",musique1, "https://upload.wikimedia.org/wikipedia/fr/5/58/LES_ENFOIR%C3%89S_-_2012.png",bibliothèque));
            return bibliothèque;
        }

        public static Bibliothèque CreerBibliothequeGroupe()
        {
            Bibliothèque bibliothèque = new Bibliothèque(2);
            Musique musique1 = new Musique("Rap");
            Musique musique2 = new Musique("Hip-Hop");
            Musique musique3 = new Musique("Electro");

            bibliothèque.AjouterElement(new Groupe("Le groupe publie son premier album studio intitulé L'École des points vitaux en mars 2010. Il suit d'un deuxième album studio intitulé L'Apogée, qui vaut à Sexion d'Assaut deux NRJ Music Awards le 26 janvier 2013, dans les catégories « groupe/duo francophone de l'année » et « chanson francophone de l'année » pour Avant qu'elle parte. À la suite de l'immense succès de L'Apogée, tous les membres du groupe décident de se lancer dans une carrière solo.","Sexion d'assaut",musique1, "https://e-cdns-images.dzcdn.net/images/artist/f0022808c3aeb889fbe8d77227828305/500x500.jpg",bibliothèque));
            bibliothèque.AjouterElement(new Groupe("Le groupe a publié deux albums (Twenty One Pilots en 2009 et Regional at Best en 2011) avant de signer avec le label Fueled by Ramen en 20121. Le premier album créé avec ce nouveau label, Vessel sort en 2013, il fut un succès. Le second, Blurryface , sort en 2015 et fait connaître le groupe au monde entier grâce à des millions de vues sur YouTube. Le 17 décembre 2016, ils sortent un EP comprenant 5 titres de Blurryface enregistré en live-session avec le groupe MuteMath, nommé TØP×MM. Le 5 octobre 2018 sort leur cinquième album studio s’intitulant Trench", "Twenty One Pilots", musique2, "https://media.virginradio.fr/article-4125097-facebook-f5/twenty-one-pilots-soundcheck.jpg", bibliothèque));
            bibliothèque.AjouterElement(new Groupe("Daft Punk (prononcé en anglais : [dæft pʌŋk]), est un groupe de musique électronique français, originaire de Paris. Composé de Thomas Bangalter et Guy-Manuel de Homem-Christo, le groupe est actif depuis 1993, et participe à la création et à la démocratisation du mouvement de musique électronique baptisé french touch. Ils font partie des artistes français s'exportant le mieux à l'étranger, et ce depuis la sortie de leur premier véritable succès, Da Funk en 1995.", "Daft Punk", musique3, "https://1more.fr/artists/655_1492176441.jpg", bibliothèque));

            return bibliothèque;
        }
        
        public static Bibliothèque CreerBibliothequeEvenement()
        {
            Bibliothèque bibliothèque = new Bibliothèque(4);
            Musique musique = new Musique("EDM");
            Musique musique1 = new Musique("Rap/Rock");

            bibliothèque.AjouterElement(new Evenement("A-t-on encore besoin de présenter Tomorrowland, le plus grand festival de dance au monde, qui accueille chaque année une programmation de la crème des DJs de la dance dans la ville Belge de Boom ? Les accros de l'EDM, house, techno et du hardstyle savent déjà qu'ils n'y trouveront que le meilleur de l'électro ! Cette année, le festival souffle 15 bougies. L'occasion de ressuciter le thème de 2012, qui avait été adoré par les fans avec The Book of Wisdom The Return.", "Tomorrowland 2019", musique, "https://www.eventdestination.net/wp-content/uploads/2018/10/tomorrowland-2019-ticket-pacchetti-hotel-camping.jpg", bibliothèque));
            bibliothèque.AjouterElement(new Evenement("Les Vieilles Charrues est le plus grand festival de France ! Avec son programme aux allures de kaléidoscope musical, chacun trouvera de quoi s'enthousiasmer au festival breton, qui accueille chaque année des centaines de milliers de fans à Carhaix. Les quatre jours du festival seront illuminés des sons de légendes du rock, de géants de la dance et de stars montantes.Un événement enivrant pour tous les musicophiles!", "Vieilles Charrues 2019", musique1, "http://rennes.aujourdhui.fr/uploads/assets/evenements/recto_flyer/2019/07/42640_festival-les-vieilles-charrues.jpg", bibliothèque));

            return bibliothèque;
        }




    }
}
