﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.Text;

namespace Model
{
    [Serializable]
    public class Association : Element
    {
        public ObservableCollection<Artiste> SesArtistes { get; private set; }
        public ObservableCollection<Groupe> SesGroupes { get; private set; }

        public ObservableCollection<Evenement> SesEvenements { get; private set; }

        public Association(String Description, String Nom, Musique Style, String Image, Bibliothèque bibliothèque)
            : base(Description,Nom,Style,Image, bibliothèque)
        {
            SesArtistes = new ObservableCollection<Artiste>();
            SesEvenements = new ObservableCollection<Evenement>();
            SesGroupes = new ObservableCollection<Groupe>();
        }

        public void SetArtiste(Artiste artiste)
        {
            SesArtistes.Add(artiste);
        }
       public void SetEvenement(Evenement evenement)
        {
            SesEvenements.Add(evenement);
        }
        public void SetGroupe(Groupe groupe)
        {
            SesGroupes.Add(groupe);
        }

        public override string ToString()
        {
            String message = $"Association : {Nom}\n    Description : {Description}\n   Style de Musqiue : {StyleDeMusique.ToString()}";
            return message;
        }
    }
}
