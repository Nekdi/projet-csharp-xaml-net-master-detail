﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Model
{
    [Serializable]
    public class Groupe : Element
    {

        public ObservableCollection<Artiste> SesArtistes { get; private set; }

        public ObservableCollection<Association> SesAssociations { get; private set; }
        public ObservableCollection<Evenement> SesEvenements { get; private set; }


        public Groupe(String Description, String Nom, Musique Style, String Image, Bibliothèque bibliothèque)
            : base(Description, Nom,Style,Image,bibliothèque)
        {
            SesArtistes = new ObservableCollection<Artiste>();
            SesAssociations = new ObservableCollection<Association>();
            SesEvenements = new ObservableCollection<Evenement>();
        }

        public void SetArtiste(Artiste artiste)
        {
            SesArtistes.Add(artiste);
        }
        public void SetAssociation(Association association)
        {
            SesAssociations.Add(association);
        }
        public void SetEvenement(Evenement evenement)
        {
            SesEvenements.Add(evenement);
        }


        public override string ToString()
        {
            String message = $"Groupe : {Nom}\n  =  Description : {Description}\n  = Style de Musqiue : {StyleDeMusique.Nom}\n\n";
            return message;
        }

    }
}
