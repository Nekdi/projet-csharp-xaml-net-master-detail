﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;

namespace Model
{
    [Serializable]
    public class Musique
    {
        public String  Nom { get; private set; }
        public  ObservableCollection<Element> LesElements { get; private set; }

        public Musique(String Nom)
        {
            LesElements = new ObservableCollection<Element>();            
            this.Nom = Nom;
        }

        public override string ToString()
        {
            String message = $"====={Nom}=====\n";
            foreach(Element element in LesElements)
            {
                
                message = message + element.ToString() + "\n" ;
            }
            return message;
        }


    }
}
