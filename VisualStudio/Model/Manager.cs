﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Model
{
    [Serializable]
    public class Manager : IPersistance
    {
        public List<Bibliothèque> LesBibliotheques { get; set; }

        public Manager()
        {
            LesBibliotheques = new List<Bibliothèque>();
        }

        public void Sauvegarde()
        {
            List<Bibliothèque> MesBiliothèques = new List<Bibliothèque>();
            foreach(Bibliothèque bibliothèque in LesBibliotheques)
            {
                MesBiliothèques.Add(bibliothèque);
            }

            using (Stream stream = File.Open("data.bin", FileMode.Create))
            {
                BinaryFormatter bin = new BinaryFormatter();
                bin.Serialize(stream, MesBiliothèques);
            }
        }

        public void Chargement()
        {
           if (File.Exists("data.bin"))
            {
                using (Stream stream = File.Open("data.bin", FileMode.Open))
                {
                    BinaryFormatter bin = new BinaryFormatter();

                    var Contenu = (List<Bibliothèque>)bin.Deserialize(stream); 
                    foreach(Bibliothèque uneBiliotheque in Contenu)
                    {
                        for(int i=0; i < LesBibliotheques.Count; i++)
                        {
                            if (uneBiliotheque.identifiant == LesBibliotheques[i].identifiant)
                            {
                                if (uneBiliotheque.LesMusiques.Count > 0)
                                {
                                    int j = LesBibliotheques.IndexOf(LesBibliotheques[i]);
                                    LesBibliotheques[j] = uneBiliotheque;
                                }
                            }
                        }

                    }
                }
            }
        }

    }
}
