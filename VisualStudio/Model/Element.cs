﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.Text;

namespace Model
{
    [Serializable]
    public class Element
    {
        public String Nom { get; private set; }

        public Musique StyleDeMusique { get; private set; }

        public Bibliothèque SaBibliotheque { get; private set; }

        public String Description { get; private  set; }

        public String Image { get; private set; }

        public ObservableCollection<Reseaux> LesReseaux { get; private set; }



        public Element(String Description, String Nom, Musique musique, String Image,Bibliothèque bibliothèque)
        {
            this.Nom = Nom;
            this.Description = Description;
            this.StyleDeMusique = musique;
            this.Image = Image;
            this.SaBibliotheque = bibliothèque;
            LesReseaux = new ObservableCollection<Reseaux>();
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override string ToString()
        {
            return $"{Nom}";
        }
    }
}
