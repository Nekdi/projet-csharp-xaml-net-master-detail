﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Model
{
    [Serializable]
     public class Bibliothèque
    {
        public ObservableCollection<Musique> LesMusiques { get; private set; }

        public int identifiant { get; private set; }

        public Bibliothèque(int identifiant)
        {
            LesMusiques = new ObservableCollection<Musique>();
            this.identifiant = identifiant;
        }

        public void AjouterElement(Element element)
        {
            if (LesMusiques.Contains(element.StyleDeMusique))
            {
                int i = LesMusiques.IndexOf(element.StyleDeMusique);
                LesMusiques[i].LesElements.Add(element);                
            }
            else
            {
                LesMusiques.Add(element.StyleDeMusique);
                int i = LesMusiques.IndexOf(element.StyleDeMusique);
                LesMusiques[i].LesElements.Add(element);               
            }        
        }

        public void SupprimerElement(Element element)
        {
            int i = LesMusiques.IndexOf(element.StyleDeMusique);
            LesMusiques[i].LesElements.Remove(element);
        }



        public override string ToString()
        {
            String message = "Bibliothèque\n";

            foreach(Musique musique in LesMusiques)
            {
                message = message + musique.ToString()+"\n";
            }
            return message;
        }

    }
}
