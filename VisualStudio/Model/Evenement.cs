﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Model
{
    [Serializable]
    public class Evenement : Element
    {

        public ObservableCollection<Artiste> SesArtistes { get; private set; }

        public ObservableCollection<Association> SesAssociations { get; private set; }

        public ObservableCollection<Groupe> SesGroupes { get; private set; }

        public Evenement(String Description, String Nom, Musique Style, String Image, Bibliothèque bibliothèque)
            :base(Description,Nom,Style,Image, bibliothèque)
        {
            SesArtistes = new ObservableCollection<Artiste>();
            SesAssociations = new ObservableCollection<Association>();
            SesGroupes = new ObservableCollection<Groupe>();
        }


        public void SetArtiste(Artiste artiste)
        {
            SesArtistes.Add(artiste);
        }
        public void SetAssociation(Association association)
        {
            SesAssociations.Add(association);
        }
        public void SetGroupe(Groupe groupe)
        {
            SesGroupes.Add(groupe);
        }

        public override string ToString()
        {
            String message = $"Evenement : {Nom}\n    Description : {Description}\n   Style de Musqiue : {StyleDeMusique.ToString()}";
            return message;
        }
    }
}
