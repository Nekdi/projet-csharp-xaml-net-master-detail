﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;

namespace Model
{
    [Serializable]
    public class Artiste : Element
    {    

        public Groupe SonGroupe { get; private set; }

        public ObservableCollection<Association> SesAssociations { get; private set; }

        public ObservableCollection<Evenement> SesEvenements { get; private set; }



        public Artiste(String Description, String Nom, Musique Style, String Image, Bibliothèque bibliothèque)
            : base(Description, Nom, Style,Image,bibliothèque)
        {
            SesAssociations = new ObservableCollection<Association>();
            SesEvenements = new ObservableCollection<Evenement>();
        }
        
        public void SetGroupe(Groupe groupe)
        {
            SonGroupe = groupe;
        }
        public void SetAssociation(Association association)
        {
            SesAssociations.Add(association);
        }
        public void SetEvenement(Evenement evenement)
        {
            SesEvenements.Add(evenement);
        }

        public override string ToString()
        {
            String message = $"Artiste : {Nom}\n  =  Description : {Description}\n  =  Style de Musqiue : {StyleDeMusique.Nom}\n\n";
            return message;
        }

    }
}
