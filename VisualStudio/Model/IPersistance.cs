﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    interface IPersistance
    {
        void Sauvegarde();
        void Chargement();
    }
}
